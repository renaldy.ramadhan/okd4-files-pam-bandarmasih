$TTL    604800
@       IN      SOA     okd4-services.pdambjm.com. admin.pdambjm.com. (
                  1     ; Serial
             604800     ; Refresh
              86400     ; Retry
            2419200     ; Expire
             604800     ; Negative Cache TTL
)

; name servers - NS records
    IN      NS      okd4-services

; name servers - A records
okd4-services.pdambjm.com.          IN      A       192.168.9.20

; OpenShift Container Platform Cluster - A records
bastion.okd.pdambjm.com.         IN      A      192.168.9.20
bootstrap.okd.pdambjm.com.        IN      A      192.168.9.21
master-1.okd.pdambjm.com.        IN      A      192.168.9.22
master-2.okd.pdambjm.com.         IN      A      192.168.9.23
master-3.okd.pdambjm.com.         IN      A      192.168.9.24
worker-1.okd.pdambjm.com.        IN      A      192.168.9.25
worker-2.okd.pdambjm.com.        IN      A      192.168.9.26
worker-3.okd.pdambjm.com.        IN      A      192.168.9.27

; OpenShift internal cluster IPs - A records
api.okd.pdambjm.com.    IN    A    192.168.9.20
api-int.okd.pdambjm.com.    IN    A    192.168.9.20
*.apps.okd.pdambjm.com.    IN    A    192.168.9.20

